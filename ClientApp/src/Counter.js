import React, { Component } from 'react';
// import { Route } from 'react-router';
// import { Layout } from './components/Layout';
// import { Home } from './components/Home';
// import { FetchData } from './components/FetchData';
// import { Counter } from './components/Counter';

// import './custom.css'

export default class Counter extends Component {
  static displayName = Counter.name;

  constructor(props) {
    super(props);
    this.state = { currentCount: 0 };
    this.incrementCounter = this.incrementCounter.bind(this);
    this.resetCounter = this.resetCounter.bind(this);
    this.decrementCounter = this.decrementCounter.bind(this);
  }

  decrementCounter() {
    this.setState({
      currentCount: this.state.currentCount - 1
    });
  }

  resetCounter() {
    this.setState({
      currentCount: 0
    });
  }

  incrementCounter() {
    this.setState({
      currentCount: this.state.currentCount + 1
    });
  }

  render() {
    return (
      <div>
        <h1>Counter</h1>

        <p>This is a simple example of a React component.</p>

        <p aria-live="polite">Current count: <strong>{this.state.currentCount}</strong></p>

        <button className="btn btn-primary" onClick={this.decrementCounter}>Decrease</button>
        <button className="btn btn-primary" onClick={this.resetCounter}>Reset</button>
        <button className="btn btn-primary" onClick={this.incrementCounter}>Increase</button>
      </div>
    );
  }
}
